[[Содержание]](../../index.md)

# Проверочная работа №1

[[Назад]](../atomic_classes/atomic_classes.md)  [[Вперед]](../thread_local/thread_local.md)

<div dir="auto">
	<h3>Задание 1. Проверочная работа 1.1</h3>
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Задача 1</h2>
		<p>Класс&nbsp;
			<code spellcheck="false">course.concurrency.exams.Auction</code>принимает заявки от потенциальных покупателей.&nbsp;Для упрощения задачи рассмотрим борьбу только за один лот.
		</p>
		<p>Что происходит:</p>
		<ul>
			<li>
				<p>Когда участник аукциона выставляет заявку, он вызывает метод&nbsp;
					<code spellcheck="false">propose</code>
				</p>
			</li>
			<li>
				<p>Если цена в новой заявке ниже, чем&nbsp;
					<code spellcheck="false">latestBid</code>, она отбрасывается, а метод propose возвращает false
				</p>
			</li>
			<li>
				<p>Если цена в новой заявке выше, чем&nbsp;
					<code spellcheck="false">latestBid</code>, новая заявка становится&nbsp;
					<code spellcheck="false">latestBid</code>. Владельцу предыдущей&nbsp;
					<code spellcheck="false">latestBid</code> приходит уведомление, что его заявка устарела. Метод propose в случае обновления заявки возвращает true
				</p>
			</li>
			<li>
				<p>Метод&nbsp;
					<code spellcheck="false">sendOutdatedMessage </code>выполняется 2 секунды
				</p>
			</li>
		</ul>
		<p>Что нужно сделать:</p>
		<p>Переписать класс&nbsp;
			<code spellcheck="false">Auction</code> для работы в многопоточной среде:
		</p>
		<ol>
			<li>
				<p>Реализовать требования с помощью пессимистичной блокировки в классе 
					<code spellcheck="false">AuctionPessimistic</code>
				</p>
			</li>
			<li>
				<p>Реализовать требования с помощью оптимистичной блокировки в классе 
					<code spellcheck="false">AuctionOptimistic</code>
				</p>
			</li>
			<li>
				<p>Адаптировать 
					<code spellcheck="false">Notifier</code> для неблокирующей отправки сообщений
				</p>
			</li>
			<li>
				<p>Сравнить время выполнения задачи с помощью теста 
					<code spellcheck="false">AuctionTests</code>
				</p>
				<p>(в тесте ничего менять не нужно)</p>
			</li>
		</ol>
	</div>
</div>



<div dir="auto">
	<h3> Задание 2. Проверочная работа 1.2</h3>
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Задача 2</h2>
		<p>К предыдущим требованиям прибавляется ещё одно:</p>
		<p>Метод&nbsp;
			<code spellcheck="false">stopAuction</code>останавливает аукцион и возвращает последнюю принятую заявку. После вызова приём заявок останавливается.
		</p>
		<p>Что нужно сделать:</p>
		<ol>
			<li>
				<p>Реализовать требования с помощью пессимистичной блокировки в классе 
					<code spellcheck="false">AuctionStoppablePessimistic</code>
				</p>
			</li>
			<li>
				<p>Реализовать требования с помощью оптимистичной блокировки в классе 
					<code spellcheck="false">AuctionStoppableOptimistic</code>
				</p>
			</li>
			<li>
				<p>Сравнить корректность и время выполнения задачи с помощью теста 
					<code spellcheck="false">AuctionStoppableTests</code>
				</p>
				<p>(в тесте ничего менять не нужно)</p>
			</li>
		</ol>
	</div>
</div>

<div>
	<div dir="auto">
		<div dir="auto">
			<div contenteditable="false">
				<p>Сначала реализуйте задачу самостоятельно!</p>
				<p>Затем можно свериться с примером ниже:</p>
			</div>
		</div>
	</div>
</div>

[Auction_implementation.patch (7.1 kB)](../../patches/Auction_implementation.patch)