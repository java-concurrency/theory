[[Содержание]](../../index.md)

# Lock и его реализации

[[Назад]](../synchronized/synchronized.md)  [[Вперед]](../deadlock_livelock_starvation/deadlock_livelock_starvation.md)

![locks](locks.mp4)

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h3>Обнаружение дедлоков</h3>
		<p>Ожидание на локах переводит поток в статус WAITING. Системы мониторинга и консольные утилиты могут по-разному реагировать на потоки в этом состоянии.</p>
		<p>Например, утилита&nbsp;
			<code spellcheck="false">jstack</code>проигнорирует дедлоки у таких потоков. Чтобы включить в анализ потоки в статусах WAITING,&nbsp;
			<em>jstack&nbsp;</em>нужно запускать с ключом -l (эль маленькая).
		</p>
		<p>Похожая ситуация для других инструментов - многие из них по умолчанию реагируют только на BLOCKED потоки.</p>
	</div>
</div>

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Дополнительные материалы</h2>
	<p>1.&nbsp;
		<a href="https://www.youtube.com/watch?v=c3_j5cPcRAI" rel="noopener noreferrer nofollow">Using Java 8 Lambdas and Stampedlock to manage thread safety by Heinz M. Kabutz</a>&nbsp;(англ,&nbsp;47:27)
	</p>
	<p>Обзор StampedLock и несколько приёмов по работе с ним,&nbsp;включая конвертации типов локов.</p>
	<p>2.&nbsp;
		<a href="https://programmer.help/blogs/implementation-principle-of-reentrantlock.html" rel="noopener noreferrer nofollow">Внутреннее устройство ReentrantLock</a>&nbsp;(англ)
	</p>
	<p>В следующих лекциях мы немного затронем строение RL,&nbsp;но кто хочет знать максимум деталей - вам сюда.</p>
	<p>3.&nbsp;
		<a href="https://web.archive.org/web/20210414205911/https://blogs.oracle.com/dave/javautilconcurrent-reentrantlock-vs-synchronized-which-should-you-use" rel="noopener noreferrer nofollow">Сравнение внутренних механизмов&nbsp;ReentrantLock и&nbsp;synchronized</a>&nbsp;(англ)
	</p>
	<p>Сотрудник Oracle рассказывает про разницу между инструментами.&nbsp;</p>
	<p>Итог: реализованы они вообще по-разному,&nbsp;но производительность почти не отличается</p>
</div>