[[Содержание]](../../index.md)

# Классы Atomic

[[Назад]](../volatile/volatile.md)  [[Вперед]](../home_work_1/home_work_1.md)

![atomic_classes](atomic_classes.mp4)

<div contenteditable="false">
<h2>Неблокирующие алгоритмы: очень кратко</h2>
<p>Тема относится к&nbsp;разработке новых структур данных и&nbsp;алгоритмов. Для энтерпрайза все базовые структуры уже реализованы в&nbsp;библиотеках, поэтому можно сильно не&nbsp;погружаться.</p>
<h3>Блокирующие алгоритмы</h3>
<p>Код называется блокирующим, если прогресс потока зависит от&nbsp;действий других потоков:</p>
<ul>
<li>
<p>Если поток застрял в&nbsp;synchronized методе, то&nbsp;другие потоки не заходят в&nbsp;другие synchronized методы.</p>
</li>
<li>
<p>Если поток делает запрос в&nbsp;БД, то&nbsp;прогресс зависит скорости работы БД.</p>
</li>
<li>
<p>При вызове t1.join() поток ждёт, пока завершится метод внутри t1.</p>
</li>
</ul>
<p>Блокирующими считаются&nbsp;</p>
<p>⛔️ Критические секции: блоки synchronized, ReentrantLock<br />⛔️ Общение с&nbsp;внешним миром: запросы в&nbsp;БД, HTTP-запросы, работа с&nbsp;файлами<br />⛔️ Любая другая&nbsp;зависимость от&nbsp;работы других потоков:</p>
<pre><code spellcheck="false">AtomicInteger value = new AtomicInteger(0);
...
while (!value.compareAndSet(5, 10)) {
 Thread.yield();
}</code></pre>
</div>
<p>Поток не&nbsp;продвинется дальше, пока кто-нибудь не&nbsp;обновит значение до&nbsp;5. Это тоже можно считать блокирующим кодом.&nbsp;</p>
<p>Можно не&nbsp;считать. Суть не в терминах,&nbsp;а в проблеме:)</p>
<p>✅ Простой код<br />❌ Узкое место системы<br />❌ Вероятность&nbsp;дедлока</p>
<hr contenteditable="false" />
<h3>Неблокирующие алгоритмы</h3>
<p>Используют другой подход к&nbsp;организации кода в многопоточной среде. Бывают трёх типов:</p>
<p>1)&nbsp;obstruction-free&nbsp;</p>
<p>Поток продолжит выполнение, если притормозить остальные потоки. Очень слабая гарантия.&nbsp;</p>
<p>2) lock-free&nbsp;</p>
<p>Всегда как минимум один поток прогрессирует. Основной инструмент&nbsp;&mdash; CAS операции в классах Atomic*</p>
<pre><code spellcheck="false">int localVar = atomicVar.get();
while (!atomicVar.compareAndSet(localVar, localVar + 5)) {
 localVar = atomicVar.get();
}&nbsp;</code></pre>
</div>
<p>✅ Нет&nbsp;дедлоков&nbsp;</p>
<p>❌ Поток может&nbsp;долго не продвигаться в работе</p>
<p>❌&nbsp;Большая нагрузка на&nbsp;процессор при высокой конкуренции</p>
<p>❌ Сложная разработка, особенно если переменных несколько&nbsp;</p>
<p>❌ ABA проблема&nbsp;</p>
<p>3) wait-free&nbsp;</p>
<p>Гарантия, что каждый поток продвинется в&nbsp;своём выполнении за&nbsp;конечное число шагов. Сделать быстрые алгоритмы с&nbsp;такими гарантиями сложно. Например,&nbsp;для wait-free очереди количество потоков должно быть постоянно и&nbsp;заранее известно. Понятно, что это очень ограниченные условия.</p>
<hr contenteditable="false" />
<p>В энтерпрайзе обычно стоит выбор между критическими секциями и lock-free структурами.&nbsp;Однозначно сказать, что один вариант лучше другого&nbsp;&mdash; нельзя.&nbsp;Производительность lock-free и&nbsp;критических секций может меняться для разных алгоритмов и при разной&nbsp;нагрузке.&nbsp;</p>
</div>

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>
			</button>Дополнительные материалы
		</h2>
		<p>1.&nbsp;
			<a href="http://chaoran.me/assets/pdf/wfq-ppopp16.pdf" rel="noopener noreferrer nofollow">Wait-free Queue as Fast as Fetch-and-Add</a>&nbsp;(англ)
		</p>
		<p>Самая быстрая реализация wait-free очереди</p>
		<p>2.&nbsp;
			<a href="https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D1%82%D0%B5%D0%BA_%D0%A2%D1%80%D0%B0%D0%B9%D0%B1%D0%B5%D1%80%D0%B0" rel="noopener noreferrer nofollow">Стек Трайбера</a>&nbsp;
		</p>
		<p>Неблокирующая реализация стека,&nbsp;которая используется во внутренних структурах экзекьюторов&nbsp;и CompletableFuture</p>
	</div>
</div>