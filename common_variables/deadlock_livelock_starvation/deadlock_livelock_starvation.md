[[Содержание]](../../index.md)

# Deadlock, livelock, starvation

[[Назад]](../locks/locks.md)  [[Вперед]](../volatile/volatile.md)

![deadlock_livelock_starvation](deadlock_livelock_starvation.mp4)

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Дополнительные материалы</h2>
		<p>
			<a href="https://deadlockempire.github.io/#menu" rel="noopener noreferrer nofollow">Игра The Deadlock Empire</a>
		</p>
		<p>Здесь вы играете за планировщик потоков - нужно выполнить код так, чтобы получился дедлок. На последних уровнях отлично прокачивает навыки мысленной имитации гонок</p>
	</div>
</div>
<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h3>Задание 1. Практика: имитация дедлоков</h3>
<div contenteditable="false">
	<p>Вам понадобится исходный код 
		<a href="https://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/concurrent/ConcurrentHashMap.java" rel="noopener noreferrer nofollow">ConcurrentHashMap</a>.
	</p>
	<ol>
		<li>
			<p>Изучите механизмы синхронизации (если они есть) в методах:</p>
		</li>
	</ol>
	<ul>
		<li>
			<p>compute</p>
		</li>
		<li>
			<p>computeIfAbsent</p>
		</li>
		<li>
			<p>merge</p>
		</li>
		<li>
			<p>putVal</p>
		</li>
		<li>
			<p>clear</p>
		</li>
	</ul>
	<ol start="2">
		<li>
			<p>Вспомните признаки дедлока</p>
		</li>
		<li>
			<p>Напишите код, используя методы выше, который может привести к дедлоку</p>
		</li>
	</ol>
</div>
	</div>
</div>