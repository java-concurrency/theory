[[Содержание]](../../index.md)

# Класс ThreadLocal

[[Назад]](../home_work_1/home_work_1.md)  [[Вперед]](../semaphore_exchanger/semaphore_exchanger.md)

![thread_local](thread_local.mp4)

<div contenteditable="false">
    <h2>Задание 1. Практика: анализ класса GridThreadSerialNumber
</h2>
	<p>Посмотрите на код класса&nbsp;
		<a href="https://github.com/apache/ignite/blob/da8a6bb4756c998aa99494d395752be96d841ec8/modules/core/src/test/java/org/apache/ignite/session/GridThreadSerialNumber.java" rel="noopener noreferrer nofollow">GridThreadSerialNumber</a>
	</p>
	<p>Зачем нужен 
		<code spellcheck="false">synchronized</code> в методе 
		<code spellcheck="false">initialValue</code>?
	</p>
	<p>Предложите более простой код для этой цели</p>
</div>

<div dir="auto">
	<div contenteditable="false">
		<p>Хотя класс ThreadLocalRandom никак не связан с ThreadLocal, всё же хочется его обсудить</p>
		<a contenteditable="false"></a>
		<h2>Генерация случайных чисел в многопоточной среде</h2>
		<p>Случайные числа в JDK можно получить 5&nbsp;способами:</p>
		<pre>
			<code spellcheck="false">
1 | Random rand = new Random();
    double r1 = rand.nextDouble();

2 | double r2 = Math.random();

3 | SecureRandom sr = new SecureRandom();
    double r5 = sr.nextDouble();

4 | ThreadLocalRandom tlr = ThreadLocalRandom.current();
    double r3 = tlr.nextDouble();

5 | SplittableRandom spr = new SplittableRandom();
    double r4 = spr.nextDouble();</code>
		</pre>
	</div>
	<ol>
		<li>
			<p>
				<code spellcheck="false">Random</code>&nbsp;- потокобезопасный класс,&nbsp;можно использовать один экземпляр на несколько потоков.
				<br />Основной метод 
				<code spellcheck="false">next</code> использует оптимистичную блокировку и CAS&nbsp;операции:
			</p>
		</li>
	</ol>
	<pre>
		<code spellcheck="false">
AtomicLong seed = this.seed;
do {
        oldseed = seed.get();
        nextseed = &hellip;;
} while (!seed.compareAndSet(oldseed, nextseed));</code>
	</pre>
</div>
<p>Любая синхронизация снижает производительность. В случае CAS просадка&nbsp;минимальная,&nbsp;но при большой конкуренции&nbsp;может стать ощутимой. Делить один экземпляр между потоками было актуально,&nbsp;когда память была очень дорогой.&nbsp;</p>
<p>В наши дни&nbsp;экземпляр генератора не занимает много памяти и не требует долгой инициализации.&nbsp;Поэтому сегодня&nbsp;Random - не самый оптимальный выбор.</p>
<ol start="2">
	<li>
		<p>
			<code spellcheck="false">Math.random()</code>&nbsp;под капотом использует 
			<code spellcheck="false">Random</code>
		</p>
	</li>
	<li>
		<p>
			<code spellcheck="false">SecureRandom</code>&nbsp;- наследник 
			<code spellcheck="false">Random</code> с более сложным алгоритмом
		</p>
	</li>
	<li>
		<p>
			<code spellcheck="false">ThreadLocalRandom</code>&nbsp;- тоже&nbsp;наследник 
			<code spellcheck="false">Random</code>,&nbsp;но&nbsp;методы родителя используются минимально.
			<br />Для каждого потока 
			<code spellcheck="false">ThreadLocalRandom</code>&nbsp;хранит&nbsp;базовое значение,&nbsp;доступ к которому&nbsp;происходит через Unsafe метод. На основе базового значения локально&nbsp;вычисляется "случайное" число. Базовое значение обновляется при каждой итерации.&nbsp;
		</p>
		<p>Потоки не взаимодействуют&nbsp;между собой, и при большой конкуренции этот класс работает лучше 
			<code spellcheck="false">Random</code>
		</p>
	</li>
	<li>
		<p>
			<code spellcheck="false">SplittableRandom</code>&nbsp;- новый класс из Java 8. По результатам&nbsp;DieHarder&nbsp;тестов обеспечивает большую "случайность",&nbsp;чем алгоритм в&nbsp;
			<code spellcheck="false">Random</code>.&nbsp;Splittable - потому,&nbsp;что в нём есть метод 
			<code spellcheck="false">split</code> для использования в fork-join процессах. Для обычного кода этот генератор тоже прекрасно подходит.
			<br />НЕ потокобезопасный и&nbsp;работает быстрее аналогов. В многопоточной среде&nbsp;используется как локальная переменная
		</p>
	</li>
</ol></div></div>