[[Содержание]](../../index.md)

# Модификатор volatile

[[Назад]](../deadlock_livelock_starvation/deadlock_livelock_starvation.md)  [[Вперед]](../atomic_classes/atomic_classes.md)

![volatile](volatile.mp4)

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Маленький бенчмарк</h2>
	<p>Сравним скорость работы&nbsp;обычной и volatile переменной:</p>
	<div>
		<div>
			<table>
				<tbody>
					<tr>
						<td></td>
						<td>
							<p>Обычная</p>
						</td>
						<td>
							<p>volatile</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>Чтение,&nbsp;нс</p>
						</td>
						<td>
							<p>5.6</p>
						</td>
						<td>
							<p>6.0</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>Запись,&nbsp;нс</p>
						</td>
						<td>
							<p>6.2</p>
						</td>
						<td>
							<p>15.9</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<p>Да, запись&nbsp;в volatile переменную занимает больше времени, чем в обычную.&nbsp;Но это несколько наносекунд, в большинстве случаев этой разницей можно пренебречь.</p>
	<p>
		<em>(Результаты на разных железках могут отличаться)</em>
	</p>
</div>

<div>
	<div dir="auto">
		<div dir="auto">
			<div contenteditable="false">
				<a contenteditable="false"></a>
				<h2>Дополнительные материалы</h2>
				<p>1.&nbsp;
					<a href="https://www.youtube.com/watch?v=ESs0bZw8hsA&amp;t=1527s" rel="noopener noreferrer nofollow">Алексей Шипилёв &mdash; Если не Unsafe, то кто: восход VarHandles</a>&nbsp;(51:10)
				</p>
				<p>Про новые виды доступов к переменной: откуда взялись и кому нужны</p>
				<p>2. Doug Lea&nbsp;
					<a href="http://gee.cs.oswego.edu/dl/html/j9mm.html" rel="noopener noreferrer nofollow">JDK 9 Memory Order Modes</a>&nbsp;(англ)
					<br />Единственная дельная статья,&nbsp;которая описывает&nbsp;новые режимы модели памяти
				</p>
				<p>(часто бывает недоступна, без VPN можно посмотреть 
					<a href="https://www.youtube.com/watch?v=ZbbKzw_DLOQ" rel="noopener noreferrer nofollow">на этом видео</a>)
				</p>
			</div>
		</div>
	</div>
</div>



