[[Содержание]](../../index.md)

# Вводная лекция

[[Назад]](../../async_tasks/virtual_threads/virtual_threads.md)  [[Вперед]](../synchronized/synchronized.md)

![common_variables_review](common_variables_review.mp4)

<div>
	<div>
		<h2>
			<span>Примечание</span>
		</h2>
		<p>В лекции и дальше на курсе я довольно свободно смешиваю термины 
			<em>гонка</em>, 
			<em>data race</em> и 
			<em>race condition. </em>В литературе и интернетных статьях часто подчёркивают разницу между data race и race condition. Например:
		</p>
		<blockquote>
			<p>A race condition occurs when the timing or order of events affects the correctness of a piece of code.</p>
			<p>A data race occurs when one thread accesses a mutable object while another thread is writing to it.</p>
		</blockquote>
		<p>На курсе будем считать, что это всё ситуации, когда результат зависит от порядка работы потоков. Иногда это приводит к ошибкам, иногда нет. То же самое касается частных случаев гонки:</p>
		<ul>
			<li>
				<p>modify-and-then (поток вмешивается в изменения другого)</p>
			</li>
			<li>
				<p>check-then-act (поток влияет на условие работы другого потока)</p>
			</li>
		</ul>
		<p>Кто-то выделяет их в отдельные категории, кто-то нет. Но при обсуждении конкретных ситуаций проблем обычно не возникает:)</p>
	</div>
</div>

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Дополнительные материалы</h2>
		<p>
			<a href="https://sourceforge.net/projects/javaconcurrenta/" rel="noopener noreferrer nofollow">Визуализация классов java.util.concurrent</a>
		</p>
		<p>Позволяет поиграться с разными инструментами. На мой взгляд, не очень понятно, что происходит, но многим нравится</p>
	</div>
</div>

<h2>Кодовая база для заданий 3 модуля:</h2>

[3rd_module.patch (48.6 kB)](../../patches/3rd_module.patch)
