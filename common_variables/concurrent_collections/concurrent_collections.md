[[Содержание]](../../index.md)

# Многопоточные коллекции

[[Назад]](../barrier/barrier.md)  [[Вперед]](../lab_work/lab_work.md)

![thread_safe_collections](thread_safe_collections.mp4)
<hr/>

![concurrent_hash_map](concurrent_hash_map.mp4)

<div>
	<div dir="auto">
		<div dir="auto">
			<div contenteditable="false">
				<a contenteditable="false"></a>
				<h2>Дополнительные материалы</h2>
				<p>1.&nbsp;
					<a href="https://habr.com/ru/post/139870/" rel="noopener noreferrer nofollow">Еще раз про skiplist&hellip;</a>
				</p>
				<p>Статья на Хабре,&nbsp;описывающая принцип работы&nbsp;ConcurrentSkipListMap</p>
			</div>
		</div>
	</div>
</div>

<div contenteditable="false">
	<h2>
    Задание 1. Практика: написание кода
  </h2>
	<p>Сервис&nbsp;
		<code spellcheck="false">RestaurantService</code> ищет ресторан по имени в методе&nbsp;
		<code spellcheck="false">getByName</code>. Также в сервисе идёт сбор статистики по частоте запросов:
	</p>
	<ul>
		<li>
			<p>поле&nbsp;
				<code spellcheck="false">stat</code> накапливает&nbsp;статистику&nbsp;
			</p>
		</li>
		<li>
			<p>метод&nbsp;
				<code spellcheck="false">addToStat</code> вызывается из метода 
				<code spellcheck="false">getByName</code> и обновляет статистику&nbsp;
			</p>
		</li>
		<li>
			<p>метод&nbsp;
				<code spellcheck="false">getStat</code>&nbsp;отдаёт текущую информацию в виде строк:
			</p>
		</li>
	</ul>
	<p>
		<em> Marcellis - 5
			<br /> Burger King - 7
		</em>
	</p>
	<p>Задание:</p>
	<ol>
		<li>
			<p>Выбрать тип для stat и реализовать методы 
				<code spellcheck="false">addToStat</code> и 
				<code spellcheck="false">printStat</code>, используя только 
				<code spellcheck="false">java.util.concurrent</code>.&nbsp;Своих классов делать не надо🙂
			</p>
		</li>
		<li>
			<p>Запустить тест 
				<code spellcheck="false">RestaurantServiceTests</code> и проверить своё решение
			</p>
		</li>
	</ol>
</div>

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h3>Реализация RestaurantService</h3>
		<p>В патче ниже представлены ДВА возможных варианта под разные случаи нагрузки.</p>
		<p>Рекомендую ознакомиться с патчем и прочитать комментарии, там есть важный нюанс по поводу выбора инструментов для задач</p>
	</div>
</div>

[restaurant_service_implementations.patch (4.0 kB)](../../patches/restaurant_service_implementations.patch)