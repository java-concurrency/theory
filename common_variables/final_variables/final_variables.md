[[Содержание]](../../index.md)

# Неизменяемые переменные

[[Назад]](../lab_work/lab_work.md)  [[Вперед]](../../data_processing/fork_join_poll_review/fork_join_poll_review.md)

![final_variables](final_variables.mp4)

<div dir="auto">
<h2>Задание 1. Практика: рефакторинг</h2>
	<div contenteditable="false">
		<p>Вы работаете над системой для интернет-магазина. Заказы поступают через сайт,&nbsp;оплачиваются через платёжный шлюз и собираются на складе. Всё это разные сервисы.</p>
		<p>Cервис&nbsp;
			<code spellcheck="false">OrderService</code>&nbsp;принимает новые заказы, информацию об оплате и готовности заказа. Когда все части&nbsp;на месте,&nbsp;передаёт заказ сервису доставки:
			<br />
			<br />
		</p>
		<div contenteditable="false">
			<img title="" src="final_variables.png" alt="" />
		</div>
		<p>Доставка должна произойти один раз!</p>
		<p>Задание: сделать рефакторинг классов 
			<code spellcheck="false">Order</code> и 
			<code spellcheck="false">OrderService</code> с использованием неизменяемых переменных. Можете&nbsp;делать&nbsp;любые&nbsp;изменения&nbsp;кроме&nbsp;cигнатуры&nbsp;методов&nbsp;
			<code spellcheck="false">createOrder</code>,&nbsp;
			<code spellcheck="false">updatePaymentInfo</code>,&nbsp;
			<code spellcheck="false">setPacked</code>и&nbsp;
			<code spellcheck="false">deliver</code>. Основная задача - снизить количество синхронизации до минимума.
		</p>
		<p>К сожалению,&nbsp;на этом учебном примере сложно понять преимущества&nbsp;функционального подхода. Здесь хорошей производительности можно достичь и с помощью изменяемых переменных. Так что просто потренируемся писать код чуть по-другому:)</p>
	</div>
</div>

[immutable_order_implementations.patch (7.1 kB)](../../patches/immutable_order_implementations.patch)