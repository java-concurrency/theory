[[Содержание]](../../index.md)

# Ключевое слово synchronized

[[Назад]](../common_variables_review/common_variables_review.md)  [[Вперед]](../locks/locks.md)

![synchronized](synchronized.mp4)

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h3>Дополнительные материалы</h3>
		<p>В лекции я рассказала&nbsp;про&nbsp;"захват&nbsp;монитора" как основу для 
			<code spellcheck="false">synchronized</code>.&nbsp;
		</p>
		<p>Для написания кода этого концепта вполне достаточно. Но&nbsp;в JVM используются три стратегии блокировки. Использование объектов ОС - лишь одна из них.</p>
		<p>Об остальных и их связи между собой можно прочитать в статье:</p>
		<p>
			<a href="https://www.alibabacloud.com/blog/lets-talk-about-several-of-the-jvm-level-locks-in-java_596090" rel="noopener noreferrer nofollow">Let's Talk about Several of the JVM-level Locks in Java</a>&nbsp;(англ)
		</p>
	</div>
</div>