[[Содержание]](../../index.md)

# CountDownLatch, CyclicBarrier, Phaser

[[Назад]](../semaphore_exchanger/semaphore_exchanger.md)  [[Вперед]](../concurrent_collections/concurrent_collections.md)

![barrier](barrier.mp4)

<div contenteditable="false">
	<p>Совет:</p>
	<p>
		<code spellcheck="false">CountDownLatch</code> основан на блокирующих методах. Как и другой блокирующий код:
	</p>
	<ul>
		<li>
			<p>его легче дебажить и искать ошибки</p>
		</li>
		<li>
			<p>он выглядит проще и приятнее</p>
		</li>
	</ul>
	<p>Для тестов и демо-классов это отличный вариант.</p>
	<p>Для продакшн кода лучше подойдёт 
		<code spellcheck="false">CompletableFuture</code>. С его помощью можно использовать ресурсы системы оптимальнее. Но код выглядит посложнее.
	</p>
</div>