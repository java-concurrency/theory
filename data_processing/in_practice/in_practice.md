[[Содержание]](../../index.md)

# ForkJoinPool на практике

[[Назад]](../fork_join_poll/fork_join_poll.md)  [[Вперед]](../../threads/threads_review/threads_review.md)

![stream_api_parallel](stream_api_parallel.mp4)
<hr/>

![concurrent_hash_map](concurrent_hash_map.mp4)

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Что с энтерпрайзом</h2>
	<p>
		<code spellcheck="false">ForkJoinPool</code> лежит в основе многих инструментов JDK:
	</p>
	<ul>
		<li>
			<p>Опция parallel() в Stream API</p>
		</li>
		<li>
			<p>CompletableFuture</p>
		</li>
		<li>
			<p>ConcurrentHashMap</p>
		</li>
		<li>
			<p>Методы Arrays.parallelSort</p>
		</li>
	</ul>
	<p>Они покрывают 95% всех задач по обработке данных&nbsp;в энтерпрайзе.&nbsp;</p>
	<p>Использовать ForkJoinPool напрямую имеет смысл,&nbsp;если в множестве данных:</p>
	<ul>
		<li>
			<p>Работа идёт одновременно с двумя&nbsp;элементами или больше (сравниваем соседние,&nbsp;ищем последовательности)</p>
		</li>
		<li>
			<p>Элементы делятся на подгруппы нетривиально (разное количество элементов,&nbsp;наложение диапазонов)</p>
		</li>
		<li>
			<p>Разная логика для разных диапазонов</p>
		</li>
		<li>
			<p>Сложная сборка результата&nbsp;</p>
		</li>
	</ul>
	<p>Cложно найти одновременно простой и&nbsp;реалистичный пример,&nbsp;где использование ForkJoinPool оправдано,&nbsp;поэтому задания на код&nbsp;в&nbsp;этом&nbsp;уроке&nbsp;нет.&nbsp;Перемножать матрицы мы точно не будем</p>
	<p>Опросив все доступные мне чаты,&nbsp;я нашла один&nbsp;пример использования&nbsp;ForkJoinPool напрямую:</p>
	<pre>
		<code spellcheck="false">
ForkJoinPool использовался&nbsp;как основа самодельного rule engine.
Сервис в фоновом режиме проверял финансовые транзакции на подозрительность.

Что было интересного:
▫️ CountedCompleter и сильная рекурсивность
▫️ Пересекающиеся диапазоны данных для поиска пар транзакций, которые&nbsp;выполняются в промежутке 30 секунд с одного аккаунта и разных девайсов. Пересечение позволяло не потерять подходящие транзакции.

Какие&nbsp;проблемы возникали:
▫️ Проверки прерывались&nbsp;при перезапуске сервиса. Причина в том,&nbsp;что&nbsp;потоки&nbsp;в&nbsp;ForkJoinPool - фоновые.
▫️ Сложности с развитием и сопровождением,&nbsp;тк&nbsp;правила часто&nbsp;меняются и дополняются.
Но&nbsp;это проблема выбранного подхода,&nbsp;а не самого ForkJoinPool.</code>
	</pre>
</div>
<p>Зачем тогда&nbsp;изучать эту тему?</p>
<p>Во-первых, у доступных&nbsp;реализаций ForkJoinTask великолепный дизайн. В&nbsp;процессе изучения хороших решений растёт насмотренность и&nbsp;формируется видение прекрасного. Ваши собственные решения тоже становятся лучше.</p>
<p>Во-вторых, остаются&nbsp;5% случаев, когда ForkJoinTask придётся писать самим. На&nbsp;этот случай отлично пригодятся приёмы из&nbsp;существующих решений и знание реализации FJP.</p></div>

<div>
	<div dir="auto">
		<div dir="auto">
			<div contenteditable="false">
				<a contenteditable="false"></a>
				<h2>Дополнительные материалы</h2>
				<p>1.&nbsp;Stream API:</p>
				<p>
					<a href="https://t.me/java_fillthegaps/66" rel="noopener noreferrer nofollow">Обзор и основные методы</a>
				</p>
				<p>
					<a href="https://t.me/java_fillthegaps/98" rel="noopener noreferrer nofollow">Как ускорить обработку данных в Stream API</a>
					<br />2.&nbsp;Урок CompletableFuture из этого курса:)
				</p>
			</div>
		</div>
	</div>
</div>





