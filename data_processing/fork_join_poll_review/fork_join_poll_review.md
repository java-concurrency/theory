[[Содержание]](../../index.md)

# ForkJoinPool для начинающих

[[Назад]](../../common_variables/final_variables/final_variables.md)  [[Вперед]](../fork_join_poll/fork_join_poll.md)

![fork_join_poll_review](fork_join_poll_review.mp4)

<div>
	<div dir="auto">
		<div dir="auto">
			<div contenteditable="false">
				<p>
					<em>&mdash; От скольки элементов можно начинать параллельную обработку?</em>
				</p>
				<p>Можно ориентироваться на формулу:</p>
				<blockquote>
					<p>Количество элементов * количество операций &gt; 10000 </p>
				</blockquote>
			</div>
		</div>
	</div>
</div>

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Дополнительные материалы</h2>
	<p>1.&nbsp;
		<a href="https://www.youtube.com/watch?v=t0dGLFtRR9c" rel="noopener noreferrer nofollow">Алексей Шипилёв &mdash; ForkJoinPool в Java 8</a>&nbsp;(1:40:00)
	</p>
	<p>Самый полный видео&nbsp;доклад по теме ForkJoinPool</p>
	<p>2.&nbsp;
		<a href="http://gee.cs.oswego.edu/dl/papers/fj.pdf" rel="noopener noreferrer nofollow">Doug Lea:&nbsp;A Java Fork/Join Framework</a>&nbsp;(англ,&nbsp;8 страниц)
	</p>
	<p>Подробное текстовое описание идеи от создателя. Особенности дизайна,&nbsp;подбор параметров,&nbsp;производительность.</p>
</div>





