[[Содержание]](../../index.md)

# ForkJoinPool для продолжающих

[[Назад]](../fork_join_poll_review/fork_join_poll_review.md)  [[Вперед]](../in_practice/in_practice.md)

![fork_join_poll](fork_join_poll.mp4)

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Дополнительные материалы</h2>
		<p>
			<a href="https://github.com/openjdk/jdk/blob/17cc7131abf699070dc491e69b21207f01c65cf6/src/java.base/share/classes/java/util/concurrent/ForkJoinPool.java#L184" rel="noopener noreferrer nofollow">ForkJoinPool Implementation Overview</a>
		</p>
		<p>Максимально подробно описан принцип работы и&nbsp;особенности&nbsp;реализации.&nbsp;Есть ссылки на дополнительные статьи и книжки по подобным структурам данных.</p>
		<p>Стоит прочитать,&nbsp;если ваш проект выполняет инфраструктурные задачи (своя БД,&nbsp;месседж брокер) и к нему можно применить подход work-stealing. В других случаях - не тратьте время,&nbsp;это слишком специфичная область знаний</p>
	</div>
</div>

<div dir="auto">
	<div contenteditable="false">
		<h2>
    Задание 1. Практика: разница экзекьюторов
  </h2>
		<p>Сравните две маленькие программы</p>
		<p>Эта&nbsp;завершится, но ничего не напечатает в консоли:&nbsp;</p>
		<pre>
			<code spellcheck="false">
public static void main(String[] args) {
    ForkJoinPool forkJoinPool = new ForkJoinPool(4);
    forkJoinPool.submit(() -&gt; System.out.println(
    IntStream.range(0,10000000).average().getAsDouble()));
}</code>
		</pre>
	</div>
	<p>Эта напечатает 49999.5, но не закончит выполнение:</p>
	<pre>
		<code spellcheck="false">
public static void main(String[] args) {
    ExecutorService executor = Executors.newCachedThreadPool();
    executor.submit(() -&gt; System.out.println(
    IntStream.range(0,10000000).average().getAsDouble()));
}</code>
	</pre>
</div>
<p>Попробуйте ответить на вопросы ниже без дебага, на основе знаний о деталях реализации:</p>
<ol>
	<li>
		<p>Почему получились разные результаты?</p>
	</li>
	<li>
		<p>Как исправить ситуацию в первом случае, чтобы и результат вывелся, и программа завершилась?</p>
	</li>
	<li>
		<p>А во втором?</p>
	</li>
</ol></div></div>








