[[Содержание]](../index.md)

# Метрики и мониторинг

[[Назад]](../testing/testing.md)  [[Вперед]](../conclusion/сonclusion.md)

![monitoring](monitoring.mp4)

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Базовый набор системных метрик</h2>
		<p>🔸&nbsp;Используемая память</p>
		<p>🔸&nbsp;Количество и скорость обращений к диску (I/O)</p>
		<p>🔸&nbsp;Загруженность процессора (CPU Utilization)</p>
		<p>ОК - до 60%</p>
		<p>Опасно - 85% и выше</p>
		<p>🔸&nbsp;Время простоя процессора (idle time)&nbsp;- обратно пропорциональна загруженности</p>
		<p>Потоки внутри процессора делятся на 2 типа:</p>
		<p>▫️ Linux: user &amp; kernel</p>
		<p>▫️ Windows:&nbsp;user &amp; privileged</p>
		<p>🔸 %user - загрузка процессора пользовательскими приложениями. Эти значения должны стремиться&nbsp;к % загруженности процессора.</p>
		<p>🔸 %sys/ %privileged - загрузка процессора задачами&nbsp;ОС. Чем ниже эти значения,&nbsp;тем лучше. Если больше 20% - что-то идёт не так.</p>
	</div>
</div>
<hr/>
<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Throughput может быть разным</h2>
	<p>Пропускная способность считается двумя способами:</p>
	<ul>
		<li>
			<p>Теоретический</p>
		</li>
	</ul>
	<p>Рассчитывается из средней скорости обработки запроса. Если запрос обрабатывается 50 мс, значит в секунду обработается 200 запросов.</p>
	<p>Такой способ используется в некоторых инструментах нагрузочного тестирования при малых нагрузках</p>
	<ul>
		<li>
			<p>Фактический, синоним - "нагрузка"</p>
		</li>
	</ul>
	<p>Общее количество обработанных запросов делится на общее время. Если за день в систему поступило 24 запроса, значит нагрузка - 1 запрос в час</p>
	<p>Такие значения показывают сервисы мониторинга</p>
</div>
<hr/>
<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Сбор системных&nbsp;метрик&nbsp;в Linux</h2>
	<p>Основные утилиты для сбора данных:</p>
	<ul>
		<li>
			<p>
				<em>top</em>,&nbsp;
				<em>mpstat</em> - общая информация для системы
			</p>
		</li>
		<li>
			<p>
				<em>pidstat</em> - статистика для конкретного процесса
			</p>
		</li>
		<li>
			<p>
				<em>iostat</em> - данные по I/O
			</p>
		</li>
		<li>
			<p>Утилита 
				<em>sar</em> собирает статистику по основным характеристикам системы (поля аналогичны 
				<em>mpstat</em>). Можно задать интервал и количество замеров:
			</p>
		</li>
	</ul>
	<p>
		<code spellcheck="false">sar 1 10</code> - 10 раз собрать статистику с интервалом 1 секунда
	</p>
	<p>Полный список полей и других полезных утилит&nbsp;
		<a href="https://github.com/sysstat/sysstat" rel="noopener noreferrer nofollow">ТУТ</a>
	</p>
	<hr/>
	<h2>Сбор системных метрик&nbsp;в&nbsp;Windows</h2>
	<p>Базовые метрики доступны в Диспетчере задач.</p>
	<p>Расширенные можно посмотреть там же. Диспетчер задач - вкладка Подробности,&nbsp;щёлк правой кнопкой - Выбрать столбцы</p>
	<p>Для сбора статистики можно использовать встроенный Системный монитор.</p>
	<p>Запускается так:</p>
	<p>1. Win +&nbsp;R</p>
	<p>2. Вводим perfmon</p>
	<p>3. Жмём ОК</p>
	<p>Как собрать и посмотреть метрики подробно описано&nbsp;
		<a href="https://tavalik.ru/performance-monitor-sborshhiki-dannyx/" rel="noopener noreferrer nofollow">ТУТ</a>.
	</p>
	<p>Для выполнения заданий ниже подойдут&nbsp;метрики из разделов</p>
	<p>Память,&nbsp;Поток, Процессор,&nbsp;Физический диск,&nbsp;Сетевой интерфейс</p>
	<p>Удобно&nbsp;выставить интервал 1 секунда</p>
</div>

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Дополнительные материалы</h2>
	<p>1.&nbsp;
		<a href="https://www.youtube.com/watch?v=EupF3VNXPPQ" rel="noopener noreferrer nofollow">Алексей Шипилёв &mdash; Performance Optimization 101</a>&nbsp;(44:49)
	</p>
	<ul>
		<li>
			<p>Ещё раз о том,&nbsp;что не всегда&nbsp;проблема в коде</p>
		</li>
		<li>
			<p>Как ставить эксперименты</p>
		</li>
		<li>
			<p>Сравнение bandwidth и latency (отличается от того,&nbsp;как я говорила в лекции. Алексей более корректно описывает эти термины,&nbsp;но на практике такая точность не всегда нужна)</p>
		</li>
	</ul>
	<p>Если не хотите смотреть полностью,&nbsp;посмотрите хотя бы&nbsp;блок&nbsp;
		<a href="https://www.youtube.com/watch?v=EupF3VNXPPQ&amp;t=1749s" rel="noopener noreferrer nofollow">Как&nbsp;увеличить производительность</a>
	</p>
	<p>2.&nbsp;
		<a href="https://shipilev.net/talks/devoxx-Nov2012-perfMethodology-mindmap.pdf" rel="noopener noreferrer nofollow">Mind-map возможных причин снижения производительности на основе данных mpstat</a>
	</p>
	<p>Рекомендуется в паре с докладом выше</p>
	<p>3.&nbsp;
		<a href="https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html" rel="noopener noreferrer nofollow">Spring Boot Actuator</a>&nbsp;(англ. документация)
	</p>
	<p>Помогает легко&nbsp;интегрировать разные системы мониторинга в приложение,&nbsp;собирать метрики и т.д. Работает через JMX и HTTP.</p>
	<p>4.&nbsp;
		<a href="https://alibaba-cloud.medium.com/a-handy-guide-to-optimizing-your-java-applications-7e254110f583" rel="noopener noreferrer nofollow">A Handy Guide to Optimizing Your Java Applications</a>&nbsp;(англ,&nbsp;41 минута чтения)
	</p>
	<p>Гигантский детальный&nbsp;гайд от Alibaba Cloud по оптимизации Java приложений</p>
	<hr contenteditable="false" />
	<a contenteditable="false"></a>
	<h4>
		<strong>Про Java Benchmark Harness</strong>
	</h4>
	<p>1.&nbsp;
		<a href="https://www.youtube.com/watch?v=8pMfUopQ9Es" rel="noopener noreferrer nofollow">Алексей Шипилёв &mdash; Java Benchmarking: как два таймстампа прочитать!</a>&nbsp;(57:28)
	</p>
	<ul>
		<li>
			<p>Почему измерять время с System.nanotime() не ок</p>
		</li>
		<li>
			<p>Почему JMH - это сложно и не всем надо. Кратко обозначены все сложности и недостатки бенчмарков</p>
		</li>
	</ul>
	<p>2.&nbsp;
		<a href="https://github.com/openjdk/jmh" rel="noopener noreferrer nofollow">Стартовая страница JMH</a>&nbsp;(англ)
	</p>
	<p>В разделе Samples есть&nbsp;подробные гайды,&nbsp;заменяющие документацию</p>
	<p>3.&nbsp;
		<a href="https://psy-lob-saw.blogspot.com/p/jmh-related-posts.html" rel="noopener noreferrer nofollow">Серия статей&nbsp;про JMH</a>&nbsp;(англ)
	</p>
	<p>Обязательно к прочтению,&nbsp;если&nbsp;решите серьёзно использовать JMH для принятия решений. Как работает библиотека,&nbsp;что влияет на результаты. Есть несколько харкорных&nbsp;статей про тестирование многопоточки:)</p>
</div>

<div dir="auto">
	<h2>
    Задание 1. Практика: сбор метрик
  </h2>
	<div contenteditable="false">
		<p>Шаги:</p>
		<ol>
			<li>
				<p>Запустите сбор статистики основных метрик</p>
			</li>
			<li>
				<p>Откройте Intellij IDEA,&nbsp;дайте ей полностью загрузиться</p>
			</li>
			<li>
				<p>Остановите сбор статистики</p>
			</li>
		</ol>
		<p>Задание:</p>
		<ol>
			<li>
				<p>Оцените изменение метрик: как загружался процессор? что происходило с диском и памятью?</p>
			</li>
			<li>
				<p>Предположите, что происходит внутри IDEA на старте</p>
			</li>
		</ol>
	</div>
</div>

