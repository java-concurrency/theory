[[Содержание]](../../index.md)

# Реактивное программирование

[[Назад]](../queue/queue.md)  [[Вперед]](../home_work_2/home_work_2.md)

![reactive_programming_1](reactive_programming_1.mp4)
<hr/>

![reactive_programming_2](reactive_programming_2.mp4)

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Дополнительные материалы</h2>
	<p>1.&nbsp;<a href="https://habr.com/ru/company/domclick/blog/504304/" rel="noopener noreferrer nofollow">Путь самурая: от Servlet к Reactive Programming</a>
	</p>
	<p>Статья на Хабре о реальном опыте перехода на реактивный стек</p>
    <br/>
    <p>2.&nbsp;<a href="https://www.youtube.com/watch?v=tjp8pTOyiWg&amp;t=631s" rel="noopener noreferrer nofollow">Максим Гореликов &mdash; Дизайн реактивной системы на Spring 5/Reactor</a>&nbsp;(58:02)
	</p>
	<p>Простые примеры переписывания кода с разбором основных ошибок</p>
    <br/>
    <p>3.<a href="https://technology.amis.nl/software-development/performance-and-tuning/spring-blocking-vs-non-blocking-r2dbc-vs-jdbc-and-webflux-vs-web-mvc/" rel="noopener noreferrer nofollow">Spring: Blocking vs non-blocking: R2DBC vs JDBC and WebFlux vs Web MVC</a>&nbsp;(англ)
		<br />Сравнение производительности традиционного и реактивного стека Spring.
	</p>
	<p>Кратко - реактивность побеждает:)</p>
    <br/>
	<p>4.&nbsp;<a href="https://habr.com/ru/post/500446/" rel="noopener noreferrer nofollow">Пара слов про R2DBC и PostgreSQL</a>
	</p>
	<p>Обзор реактивных драйверов для PostgreSQL</p>
    <br/>
	<p>5.&nbsp;<a href="https://netflixtechblog.com/system-architectures-for-personalization-and-recommendation-e081aa94b5d8" rel="noopener noreferrer nofollow">System Architectures for Personalization and Recommendation</a>&nbsp;(англ)
	</p>
	<p>Полная статья про реактивную&nbsp;систему рекомендаций Нетфликса. Но в тексте нет ни слова про реактивность и ни одного сниппета кода.&nbsp;</p>
	<p>Основной вывод: важнее продумать,&nbsp;как,&nbsp;когда и кем обрабатываются данные,&nbsp;чем просто добавить фреймворки</p>
    <br/>
	<p>6.&nbsp;<a href="https://medium.com/@owencm/reactive-web-design-the-secret-to-building-web-apps-that-feel-amazing-b5cbfe9b7c50" rel="noopener noreferrer nofollow">Reactive Web Design: The secret to building web apps that feel amazing</a>&nbsp;(англ)
	</p>
	<p>Более "приземлённая" статья про постепенную загрузку данных в мобильном приложении и Responsive Design</p>
    <br/>
	<p>7.&nbsp;<a href="https://engineering.linkedin.com/blog/2018/01/now-you-see-me--now-you-dont--linkedins-real-time-presence-platf" rel="noopener noreferrer nofollow">Now You See Me, Now You Don&rsquo;t: LinkedIn&rsquo;s Real-Time Presence Platform</a> (англ, открывается только под VPN)
	</p>
	<p>Статья LinkedIn о том, как реализованы индикаторы &ldquo;онлайн&rdquo; или &ldquo;вышел 5 минут назад&rdquo;. Реализация гораздо сложнее, чем кажется, и хорошо показывает суть реактивной архитектуры.</p>
</div>

<div dir="auto">
	<div contenteditable="false">
		<h2>
    Задание 1. Практика: реактивность и текущий проект
  </h2>
		<p>Поделитесь опытом или своими размышлениями:)</p>
		<hr contenteditable="false" />
		<p>Вариант А: у вашего рабочего проекта &ldquo;традиционная&rdquo; архитектура</p>
		<ol>
			<li>
				<p>Будет ли польза от перевода вашего проекта на реактивный стек?</p>
			</li>
			<li>
				<p>Если да - какие были бы первые шаги? И какие сложности сразу бросаются в глаза?&nbsp;</p>
			</li>
		</ol>
		<hr contenteditable="false" />
		<p>Вариант Б: рабочий проект уже использует реактивный стэк</p>
		<p>Расскажите, как вам работается на таком проекте? Какую пользу вы замечаете от реактивности? Какие сложности по сравнению с традиционным стэком?</p>
	</div>
</div>


