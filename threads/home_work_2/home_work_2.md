[[Содержание]](../../index.md)

# Проверочная работа №2

[[Назад]](../reactive_programming/reactive_programming.md)  [[Вперед]](../../testing/testing.md)

<div contenteditable="false">
	<a contenteditable="false"></a>
	<p>Попробуем применить знания,&nbsp;полученные на курсе и сделать рефакторинг многопоточного кода.</p>
	<p>
		<mark>Исходные данные</mark> (из репозитория Apache Hadoop):
	</p>
	<ul>
		<li>
			<p>Класс&nbsp;
				<a href="https://github.com/apache/hadoop/blob/03cfc852791c14fad39db4e5b14104a276c08e59/hadoop-hdfs-project/hadoop-hdfs-rbf/src/main/java/org/apache/hadoop/hdfs/server/federation/router/MountTableRefresherService.java" rel="noopener noreferrer nofollow">MountTableRefresherService</a>
			</p>
		</li>
		<li>
			<p>Класс&nbsp;
				<a href="https://github.com/apache/hadoop/blob/03cfc852791c14fad39db4e5b14104a276c08e59/hadoop-hdfs-project/hadoop-hdfs-rbf/src/main/java/org/apache/hadoop/hdfs/server/federation/router/MountTableRefresherThread.java#L34" rel="noopener noreferrer nofollow">MountTableRefresherThread</a>
			</p>
		</li>
	</ul>
	<p>
		<mark>Что происходит:</mark>
	</p>
	<p>В распределённой системе у каждого сервиса есть адрес и роутер,&nbsp;который помогает отправлять запросы в другие сервисы.</p>
	<p>Mount table - это конфигурация роутеров. Когда у сервиса что-то меняется,&nbsp;нужно оповестить другие роутеры об изменениях. Этим занимается метод 
		<code spellcheck="false">refresh()</code> класса&nbsp;
		<code spellcheck="false">MountTableRefresherService</code>
	</p>
	<p>
		<mark>Что не так:</mark>
	</p>
	<p>Используются устаревшие конструкции. Код очень громоздкий и его невозможно протестировать😢</p>
	<p>Упрощённый вариант для рефакторинга:</p>
</div>

[refactoring_task.patch (14.7 kB)](../../patches/refactoring_task.patch)

<div contenteditable="false">
	<h2>
    Задание 1. Практика: рефакторинг многопоточного кода
  </h2>
	<p>Задача:</p>
	<ol>
		<li>
			<p>Оценить многопоточные конструкции в коде, зачем они нужны и что делают</p>
		</li>
		<li>
			<p>Переписать текущий код метода 
				<code spellcheck="false">refresh</code> на более простой и понятный. Функционал должен остаться таким же, как в исходном варианте
			</p>
			<p>(оставим в стороне&nbsp;вопросы архитектуры и&nbsp;алгоритмов,&nbsp;сфокусируемся только&nbsp;на многопоточке)</p>
		</li>
		<li>
			<p>Написать тесты для новой реализации в классе 
				<code spellcheck="false">MountTableRefresherServiceTests</code>
			</p>
			<p>Заготовки для тестов там уже есть</p>
		</li>
	</ol>
</div>

[refactoring_task_implementation.patch (15.7 kB)](../../patches/refactoring_task_implementation.patch)