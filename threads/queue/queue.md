[[Содержание]](../../index.md)

# Очереди

[[Назад]](../threads_review/threads_review.md)  [[Вперед]](../reactive_programming/reactive_programming.md)

![queue](queue.mp4)

<div contenteditable="false">
	<a contenteditable="false"></a>
	<h2>Дополнительные материалы</h2>
	<p>1.&nbsp;
		<a href="https://habr.com/ru/post/112222/" rel="noopener noreferrer nofollow">Структуры данных: двоичная куча (binary heap)</a>
	</p>
	<p>Статья на хабре про структуру в основе&nbsp;PriorityBlockingQueue</p>
	<p>2.&nbsp;
		<a href="https://jack-vanlightly.com/blog/2017/12/3/rabbitmq-vs-kafka-series-introduction" rel="noopener noreferrer nofollow">RabbitMQ vs Kafka Series</a>&nbsp;(англ)
		<br />Цикл статей про разницу между месседж брокерами, где и что использовать, основные паттерны и подводные камни
	</p>
	<p>3.&nbsp;Обзорные посты на канале fill the gaps: про 
		<a href="https://t.me/java_fillthegaps/429" rel="noopener noreferrer nofollow">RabbitMQ</a> и 
		<a href="https://t.me/java_fillthegaps/430" rel="noopener noreferrer nofollow">Kafka</a>
	</p>
</div>
<div dir="auto">
	<h2>
    Задание 1. Практика: экзекьюторы и очереди
  </h2>
	<div contenteditable="false">
		<ol>
			<li>
				<p>Создайте экземпляр экзекьютора, который обрабатывает задачи по принципу LIFO</p>
			</li>
			<li>
				<p>Создайте экземпляр экзекьютора, который содержит 8 потоков и отбрасывает задачи, если нет свободных потоков для их обработки</p>
			</li>
		</ol>
	</div>
</div>








