[[Содержание]](../../index.md)

# Executors

[[Назад]](../async_tasks_review/async_tasks_review.md)  [[Вперед]](../completable_future/completable_future.md)

![executors](executors.mp4)
<div>
	<p>
		<strong>Поля конструктора ThreadPoolExecutor:</strong>
	</p>
	<div>
		<div>
			<table>
				<tbody>
					<tr>
						<td>
							<p>int corePoolSize</p>
						</td>
						<td>
							<p>Исходное и постоянное&nbsp;количество потоков</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>int maximumPoolSize</p>
						</td>
						<td>
							<p>Максимальное количество потоков</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>long keepAliveTime</p>
						</td>
						<td>
							<p>Сколько времени живёт простаивающий поток</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>TimeUnit unit</p>
						</td>
						<td>
							<p>Единица измерения времени простаивания</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>BlockingQueue&nbsp;workQueue</p>
						</td>
						<td>
							<p>Экземпляр очереди задач</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>ThreadFactory threadFactory</p>
						</td>
						<td>
							<p>Шаблон для потоков</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>RejectedExecutionHandler handler</p>
						</td>
						<td>
							<p>Что делать,&nbsp;если в экзекьютор&nbsp;нельзя добавить задачу</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<hr>
		<p>Реализации RejectedExecutionHandler:</p>
		<div>
			<div>
				<table>
					<tbody>
						<tr>
							<td>
								<p>new&nbsp;AbortPolicy()</p>
							</td>
							<td>
								<p>Отбросить&nbsp;задачу,&nbsp;выбросить&nbsp;RejectedExecutionException (вариант по умолчанию)</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>new&nbsp;CallerRunsPolicy()</p>
							</td>
							<td>
								<p>Если места в экзекьюторе нет,&nbsp;вызывающий поток решает задачу сам</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>new&nbsp;DiscardPolicy()</p>
							</td>
							<td>
								<p>Отбросить задачу</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>new&nbsp;DiscardOldestPolicy()</p>
							</td>
							<td>
								<p>Задача добавляется в конец очереди,&nbsp;а из начала выкидывается другая задача</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<p>
			<br>
			</p>
		</div>
<div>
	<div>
		<div>
			<div>
				<div>
					<h3>
						<span class="heading-content">Размер пула потоков для приложений в Docker</span>
					</h3>
					<p>Обычно на production серверах используются мощные многоядерные процессоры, где развёрнуты несколько docker образов с JVM приложениями.</p>
					<p>
						<strong>
							<mark>Проблема</mark>
						</strong>
						<br>Каждое JVM приложение видит ВСЮ доступную память и количество ВСЕХ&nbsp;доступных ядер.&nbsp;Код&nbsp;
							<code spellcheck="false">Runtime.getRuntime().availableProcessors()</code>&nbsp;активно используется и в JDK сущностях и для вычисления размера пула потоков.
							<br>В итоге каждое приложение создаёт огромный пул потоков. Несколько таких приложений заметно снижают производительность.
							</p>
							<p>
								<strong>
									<mark>Решение 1:</mark>
								</strong>
							</p>
							<p>В java 10 проблема исправлена, и JVM учитывает Docker лимиты для одного приложения. Можно спокойно использовать&nbsp;
								<em>availableProcessors()</em>&nbsp;в коде.
							</p>
							<p>Для java 8 эта поддержка добавлена с версии 8u191.</p>
							<p>В настройках Docker должны быть явно заданы&nbsp;лимиты:</p>
							<p>
								<a href="https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/tutorial-docker/html/index.html" hreftransformed="" rel="noopener noreferrer nofollow">ИНСТРУКЦИЯ&nbsp;ПО&nbsp;НАСТРОЙКЕ&nbsp;ЛИМИТОВ</a>
							</p>
							<p>
								<strong>
									<mark>Решение 2:</mark>
								</strong>
							</p>
							<p>Явно задать количество доступных процессоров флажком:</p>
							<p>
								<code spellcheck="false">-XX:ActiveProcessorCount = 4</code>
							</p>
							<p>тогда 
								<code spellcheck="false">Runtime.getRuntime().availableProcessors()</code> будет выдавать&nbsp;4
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
<div>
	<div>
		<h3>
			<span class="heading-content">Сколько потоков указывать в экзекьюторе?</span>
		</h3>
		<ol>
			<li>
				<p>
					<strong>Формула из книги “Concurrency in practice”</strong>
				</p>
				<p>N = C * (1 + WaitTime/ServiceTime)</p>
				<p>N - число потоков</p>
				<p>C - количество ядер</p>
				<p>WaitTime - время ответа внешних систем</p>
				<p>ServiceTime - время обработки запроса</p>
				<p>Формула даёт слишком большие числа в текущих реалиях, поэтому используется редко</p>
				<p>
					<br>
					</p>
				</li>
				<li>
					<p>
						<strong>Little’s Law</strong>
					</p>
					<p>N = ReqCount * ResponseTime</p>
					<p>N - количество потоков</p>
					<p>ReqCount - количество запросов в секунду</p>
					<p>ResponseTime - время обработки запроса в секундах</p>
					<p>Эта формула проще и учитывает частоту запросов, поэтому более адекватна, чем предыдущая</p>
					<p>
						<br>
						</p>
					</li>
					<li>
						<p>
							<strong>Жизнь</strong>
						</p>
						<p>На практике следует учитывать частоту запросов и срочность выполнения. В разных условиях разные параметры покажут разную пропускную способность. Обычно смотрят значения при</p>
						<p>
							<em>N = количество ядер / 2</em>
						</p>
						<p>
							<em>N = количество ядер </em>
						</p>
						<p>
							<em>N = количество ядер *2</em>
						</p>
						<p>и выбирают подходящее. Для более тонкой настройки можно запустить нагрузочный тест с подходящей нагрузкой (см. лекцию для подробностей)</p>
					</li>
				</ol>
				<p>
					<br>
					</p>
				</div>
			</div>
<div>
	<div>
		<div>
			<div>
				<span>Задание 1. Практика: эксперименты с экзекьюторами</span>
			</div>
		</div>
		<div>
			<div>
				<div>
					<div>
						<div>
							<p>Давайте посмотрим, как параметры экзекьютора влияют на пропускную способность системы.</p>
							<p>В патче из предыдущего урока вы найдёте классы для экспериментов:</p>
							<ol>
								<li>
									<p> Основной класс 
										<code spellcheck="false">ReportServiceExecutors</code>
									</p>
								</li>
							</ol>
							<p>Что там происходит:</p>
							<p>Метод 
								<code spellcheck="false">getReport</code> формирует отчёт, где комбинирует данные от двух методов - 
								<code spellcheck="false">getActiveCustomers</code> и 
								<code spellcheck="false">getItems</code>. Первый выполняется в два раза дольше, чем второй. Оба метода выполняются в отдельных потоках через экзекьютор.
							</p>
							<ol start="2">
								<li>
									<p>Класс 
										<code spellcheck="false">LoadGenerator</code> эмулирует два вида нагрузки:
									</p>
								</li>
							</ol>
							<ul>
								<li>
									<p>sleep - поток переводится в состояние WAITING. Это аналог блокирующего вызова - запроса в БД или HTTP вызова в другой сервис</p>
								</li>
								<li>
									<p>compute - вычислительная работа, поток находится в состоянии RUNNABLE</p>
								</li>
							</ul>
							<ol start="3">
								<li>
									<p>Класс 
										<code spellcheck="false">ReportServiceTests</code> представляет аналог нагрузочного теста
									</p>
								</li>
							</ol>
							<hr class="" contenteditable="false">
								<p>
									<mark>Задание</mark>:
								</p>
								<ol>
									<li>
										<p>Напишите число ядер вашего компьютера</p>
									</li>
									<li>
										<p>Поставьте в LoadGenerator метод 
											<mark>sleep</mark> и поэкспериментируйте с экзекьютором в классе 
											<code spellcheck="false">ReportServiceExecutors</code>. Попробуйте разные виды и параметры, запишите время выполнения теста при разных вариантах
										</p>
									</li>
									<li class="counter-1">
										<p>Поставьте в LoadGenerator метод 
											<mark>compute</mark> и поэкспериментируйте с экзекьютором в классе 
											<code spellcheck="false">ReportServiceExecutors</code>. Запишите время выполнения теста при разных вариантах
										</p>
									</li>
									<li>
										<p>Проанализируйте полученные цифры и напишите свои выводы и впечатления:)</p>
										<p>(приложите, пожалуйста, результаты из пунктов 2 и 3)</p>
									</li>
								</ol>
								<p>
									<br>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<div>
	<div>
		<div>
			<div>
				<span>Задание 2. Практика: изучаем экзекьюторы в Spring</span>
			</div>
		</div>
		<div>
			<div>
				<div>
					<div>
						<div>
							<p>В&nbsp;Spring есть возможность выполнить задачу асинхронно с помощью аннотаций&nbsp;
								<code spellcheck="false">@EnableAsync</code>&nbsp;и&nbsp;
								<code spellcheck="false">@Async</code> (
								<a href="https://www.baeldung.com/spring-async" rel="noopener noreferrer nofollow">инструкция</a>).&nbsp;
							</p>
							<p>Давайте посмотрим, как Spring работает с асинхронными задачами по умолчанию.</p>
							<hr>
								<h3>
									<span class="heading-content">Часть 1: Spring Core</span>
								</h3>
								<ul>
									<li>
										<p>Откройте класс 
											<a href="https://github.com/spring-projects/spring-framework/blob/main/spring-core/src/main/java/org/springframework/core/task/SimpleAsyncTaskExecutor.java" rel="noopener noreferrer nofollow">SimpleAsyncTaskExecutor</a>
										</p>
									</li>
									<li>
										<p>Посмотрите, что происходит при вызове метода 
											<code spellcheck="false">submit</code>
										</p>
									</li>
								</ul>
								<p>
									<mark>Задание</mark>: 
								</p>
								<ol>
									<li>
										<p>Опишите, как в экзекьюторе в Spring задачи распределяются по потокам</p>
									</li>
									<li>
										<p>Предположите, в каком случае имеет смысл переопределить экзекьютор по умолчанию</p>
									</li>
								</ol>
								<p>
									<br>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<div>
	<div>
		<div>
			<div>
				<span>Задание 3. Практика: изучаем экзекьюторы в Spring Boot</span>
			</div>
		</div>
		<div>
			<div>
				<div>
					<div>
						<div>
							<h3>
								<span>Часть 2: Spring Boot</span>
							</h3>
							<p>Spring Boot переопределяет многие параметры и классы, поэтому параметры экзекьютора проще узнать опытным путём</p>
							<p>
								<mark>Задание</mark>: 
							</p>
							<ol>
								<li>
									<p>Сделайте метод 
										<code spellcheck="false">AsyncClassTest.runAsyncTask</code>  асинхронным
									</p>
								</li>
								<li>
									<p>Добавьте методу 
										<code spellcheck="false">AsyncClassTest.internalTask</code> аннотацию 
										<code spellcheck="false">@Async</code>
									</p>
								</li>
								<li>
									<p>Запустите приложение и посмотрите, как задачи распределяются по потокам</p>
									<p>(убедитесь, что они запускаются не в 
										<em>main</em> потоке)
									</p>
								</li>
								<li>
									<p>Напишите особенность использования 
										<code spellcheck="false">@Async</code> аннотации, которая следует из предыдущего пункта
									</p>
								</li>
								<li>
									<p>Опишите конфигурацию экзекьютора по умолчанию в Spring Boot (бин 
										<code spellcheck="false">applicationTaskExecutor</code>)
									</p>
								</li>
								<li>
									<p>Предположите, в каком случае имеет смысл переопределить экзекьютор по умолчанию</p>
								</li>
							</ol>
							<p>
								<br>
								</p>
								<p>Факультативное задание: посмотрите 
									<a href="https://www.linkedin.com/pulse/asynchronous-calls-spring-boot-using-async-annotation-omar-ismail" rel="noopener noreferrer nofollow">в этой статье</a> 2 способа переопределить экзекьютор по умолчанию и потренируйтесь на методе 
									<code spellcheck="false">runAsyncTask</code>
								</p>
								<p>
									<br>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
