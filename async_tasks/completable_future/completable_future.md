[[Содержание]](../../index.md)

# CompletableFuture

[[Назад]](../executors/executors.md)  [[Вперед]](../virtual_threads/virtual_threads.md)

![completable_future](completable_future.mp4)

<div>
	<div>
		<h3>
			<span>Дополнительные материалы</span>
		</h3>
		<p>1.&nbsp;
			<a href="https://www.youtube.com/watch?v=-MBPQ7NIL_Y" hreftransformed="" rel="noopener noreferrer nofollow">Tomasz Nurkiewicz — CompletableFuture in Java 8, asynchronous processing done right</a>&nbsp;(1:01:09,&nbsp;англ)
		</p>
		<p>Я в лекции только обозначила основные методы,&nbsp;а здесь&nbsp;очень&nbsp;подробно показано,&nbsp;как ими пользоваться. Английский несложный, много демонстраций кода.</p>
		<p>2.&nbsp;
			<a href="https://www.youtube.com/watch?v=W7iK74YA5NM" hreftransformed="" rel="noopener noreferrer nofollow">Сергей Куксенко - Как сделать CompletableFuture еще быстрее</a>&nbsp;(49:30)
		</p>
		<p>Первая половина - обзор CF,&nbsp;начиная с 20 минуты&nbsp;- как CF используется в HTTP Client Java 11.</p>
		<p>3.&nbsp;
			<a href="https://github.com/JetBrains/intellij-community/blob/56fd73a01bf8f68633495336f851e497c024c916/java/debugger/impl/src/com/intellij/debugger/ui/tree/render/ClassRenderer.java#L198" hreftransformed="" rel="noopener noreferrer nofollow">Пример использования CompletableFuture.allOf в Intellij IDEA</a>
		</p>
		<p>Метод buildChildren показывает информацию о подклассах текущего класса. Каждый элемент собирается асинхронно, результаты объединяются через allOf</p>
	</div>
</div>

<div>
	<div>
		<div>
			<div>
				<span>Задание 1. Практика: экспериментируем с экзекьютором в CompletableFuture</span>
			</div>
		</div>
		<div>
			<div>
				<div>
					<div>
						<div>
							<p>Давайте посмотрим, как параметры экзекьютора в CF влияют на пропускную способность системы.</p>
							<p>Основной класс для экспериментов - 
								<code spellcheck="false">ReportServiceCF</code>. На всякий случай напомню:
							</p>
							<p>Метод 
								<code spellcheck="false">getReport</code> формирует отчёт, где он комбинирует данные от двух методов - 
								<code spellcheck="false">getActiveCustomers</code> и 
								<code spellcheck="false">getItems</code>. Первый выполняется в два раза дольше, чем второй. Оба метода выполняются в отдельных потоках через 
								<code spellcheck="false">supplyASync</code>.
							</p>
							<p>Класс 
								<code spellcheck="false">LoadGenerator</code> эмулирует два вида нагрузки:
							</p>
							<ul>
								<li>
									<p>
										<code spellcheck="false">sleep</code> - поток переводится в состояние WAITING. Аналог блокирующего вызова - запроса в БД или HTTP вызова в другой сервис
									</p>
								</li>
								<li class="counter-1">
									<p>
										<code spellcheck="false">compute</code> - вычислительная работа, поток находится в состоянии RUNNABLE
									</p>
								</li>
							</ul>
							<p>Класс 
								<code spellcheck="false">ReportServiceTests</code> представляет аналог нагрузочного теста
							</p>
							<hr class="" contenteditable="false">
								<p>Задание:</p>
								<ol>
									<li>
										<p>Напишите число ядер вашего компьютера</p>
									</li>
									<li class="counter-1">
										<p>Поставьте в 
											<code spellcheck="false">LoadGenerator</code> метод 
											<mark>sleep</mark> и поэкспериментируйте с экзекьютором в классе 
											<code spellcheck="false">ReportServiceCF</code>. Запишите время выполнения теста при разных вариантах
										</p>
									</li>
									<li>
										<p>Поставьте в 
											<code spellcheck="false">LoadGenerator</code> метод 
											<mark>compute</mark> и поэкспериментируйте с экзекьютором в классе 
											<code spellcheck="false">ReportServiceCF</code>. Запишите время выполнения теста при разных вариантах
										</p>
									</li>
									<li>
										<p>Проанализируйте цифры, которые у вас получились и напишите свои выводы и впечатления:)</p>
									</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div>
	<div>
		<div>
			<div>
				<p>Пример реализации Price Aggregator:</p>
				<p>(не смотрите сразу, сначала реализуйте задачу самостоятельно)</p>
			</div>
		</div>
	</div>
</div>

<div>
	<div>
		<div>
			<div>
				<span>Задание 2. Практика: возврат наименьшей цены</span>
			</div>
		</div>
		<div>
			<div>
				<div>
					<div>
						<div>
							<p>Исходный код доступен в классе 
								<code spellcheck="false">PriceAggregator</code>:
							</p>
							<ul>
								<li class="counter-1">
									<p>Метод&nbsp;
										<code spellcheck="false">getPrice</code>
										<em>&nbsp;</em>узнаёт цену на товар в конкретном магазине. Может выполняться долго (делает HTTP запрос в магазин)
									</p>
								</li>
							</ul>
							<p>Unit тесты находятся в классе 
								<code spellcheck="false"> PriceAggregatorTests</code>
							</p>
							<p>
								<strong>Задача:</strong>
							</p>
							<p>Написать метод&nbsp;
								<em>PriceAggregator</em>#
								<em>getMinPrice</em>,&nbsp;который возвращает минимальную цену на товар среди всех&nbsp;магазинов. Кто-то из них ответит быстро, а кто-то не очень. Минимальная цена выбирается из тех результатов, которые успели прийти. Если ни один магазин не вернул результат, возвращается NaN.
							</p>
							<p>Метод 
								<code spellcheck="false">getPrice</code>
								<em>&nbsp;</em> должен выполняться не более трёх&nbsp;секунд.
							</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div>
	<div>
		<div>
			<div>
				<p>Пример реализации Price Aggregator:</p>
				<p>(не смотрите сразу, сначала реализуйте задачу самостоятельно)</p>
			</div>
		</div>
	</div>
</div>

[Price_aggregator_implementation.patch (2.2 kB)](../../patches/Price_aggregator_implementation.patch)
