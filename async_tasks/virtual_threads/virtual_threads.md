[[Содержание]](../../index.md)

# Виртуальные потоки

[[Назад]](../completable_future/completable_future.md)  [[Вперед]](../../common_variables/common_variables_review/common_variables_review.md)

![virtual_threads](virtual_threads.mp4)

<div>
	<div>
		<h2>
			<span>Паттерны работы с виртуальными потоками</span>
		</h2>
		<p>(список предварительный, составлен на основе новых методов в классах JDK)</p>
		<p>Выполнение задач группой виртуальных потоков возможно двумя способами:</p>
		<ol>
			<li class="counter-1">
				<p>
					<a href="https://openjdk.java.net/jeps/428" hreftransformed="" rel="noopener noreferrer nofollow">Structured concurrency</a>
				</p>
			</li>
		</ol>
		<p>Отправить подзадачи в параллельное исполнение и забрать результат.</p>
		<div>
			<pre>
				<code spellcheck="false">
try (var scope = new StructuredTaskScope.ShutdownOnFailure()) {
    Future f1=scope.fork(…);
    Future f2=scope.fork(…);
    scope.join();
    scope.throwIfFailed();
    return f1.resultNow()+f2.resultNow();
}
                </code>
            </pre>
        </div>
        <p> В первой строке задаются правила взаимодействия подзадач:</p>
        <p>🔸
            <code spellcheck="false">ShutdownOnFailure</code> — если хотя бы одна подзадача выбросит исключение, остальные будут прерваны. Обработку прерывания всё ещё пишет разработчик, но java берёт на себя всю работу по отслеживанию и обновлению статусов
        </p>
        <p>🔸
            <code spellcheck="false">ShutdownOnSuccess</code> — когда хотя бы одна задача завершится, остальные прерываются
        </p>
        <p>
    </p>
    <p>Методы внутри блока:</p>
        <ul>
            <li>
                <p>
                    <code spellcheck="false">scope.fork</code> — задачи запускаются в едином логическом блоке
                </p>
            </li>
            <li>
                <p>
                    <code spellcheck="false">scope.join</code> — ждём завершения подзадач
                </p>
            </li>
            <li>
                <p> 
                    <code spellcheck="false">scope.throwIfFailed</code> — пробрасываем исключение, если оно возникло в одной из подзадач. Другим методом можно получить экземпляр исключения и обработать его сразу
                </p>
            </li>
            <li>
                <p>Забираем результаты через
                    <code spellcheck="false">resultNow</code> и объединяем
                </p>
            </li>
        </ul>
    <ol start="2">
        <li>
            <p>Виртуальные экзекьюторы</p>
            <p>Когда нужно выполнить подзадачи в пределах какого-то периода времени</p>
        </li>
    </ol>
    <div>
        <pre>
        <code spellcheck="false">
Future f1;
Future f2;
try (ExecutorService exec = Executor.newVirtualThreadExecutor()
    .withDeadline(Instant.now().plus(30, ChronoUnit.SECONDS)) 
) {
    f1 = exec.submit(getItems());
    f2 = exec.submit(getCustomers());
}
return f1.get() + f2.get();
        </code>
    </pre>
    </div>
<p>Что происходит:</p>
    <ul>
        <li>
            <p>У экзекьюторов появился новый метод
                <code spellcheck="false">withDeadline</code>. Если подзадачи не завершились в пределах заданного времени, то выбросится
                <code spellcheck="false">TimeoutException</code>
            </p>
        </li>
        <li>
            <p>Экзекьюторы с java 18 реализуют интерфейс
                <code spellcheck="false">Closeable</code>, поэтому их можно использовать в блоке try
            </p>
        </li>
    </ul>
    <p>Таким образом, в конструкции выше задачи будут ограничены таймаутом. </p>
    <p>Скорее всего функционал с таймаутом перейдёт и в
        <em>Structured concurrency</em>. Пока таких JEP нет, но это было бы логично:)
    </p>
    <p>
        <br>
    </p>
    </div>
</div>

<div>
	<div>
		<h2>
			<span class="heading-content">Дополнительные материалы</span>
		</h2>
		<p>1. 
			<a href="https://openjdk.org/jeps/425" hreftransformed="" rel="noopener noreferrer nofollow">JEP 425: Virtual Threads</a>
		</p>
		<p>Очень кратко о том, зачем нужны виртуальные потоки и как они будут выглядеть. Самый информативный текст среди всех статей и видео.</p>
		<p>2.&nbsp;
			<a href="https://www.youtube.com/watch?v=7GLVROqgQJY" hreftransformed="" rel="noopener noreferrer nofollow">Alan Bateman — Project loom: Modern scalable concurrency for the Java platform</a>&nbsp;(англ. с русскими субтитрами,&nbsp;1:12:52)
		</p>
		<p>Самый свежий доклад про Project Loom и&nbsp;много примеров кода.</p>
		<p>
			<a href="https://habr.com/ru/company/jugru/blog/543272/" hreftransformed="" rel="noopener noreferrer nofollow">Русская текстовая версия</a>
		</p>
		<p>3.&nbsp;
			<a href="https://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html" hreftransformed="" rel="noopener noreferrer nofollow">Project Loom: Fibers and Continuations for the Java Virtual Machine</a>&nbsp;(англ)
		</p>
		<p>Исходная формулировка для Project Loom и его обоснование. Хотя многие концепты и термины&nbsp;уже поменялись,&nbsp;общая мотивация и цель остаётся той же.</p>
		<p>4.&nbsp;
			<a href="https://www.infoq.com/podcasts/java-project-loom/" hreftransformed="" rel="noopener noreferrer nofollow">Virtual Threads and Structured Concurrency with Ron Pressler</a>&nbsp;(англ)
		</p>
		<p>Подкаст + транскрипт от техлида Project Loom</p>
		<p>Много деталей о реализации</p>
		<p>5.&nbsp;
			<a href="https://inside.java/2021/11/30/on-parallelism-and-concurrency/" hreftransformed="" rel="noopener noreferrer nofollow">On Parallelism and Concurrency</a>&nbsp;(англ)
			<br>Статья от главного разработчика Project Loom про разницу параллельности и асинхронности.
			</p>
			<p>Краткое содержание:
				<br>Цель параллельности - выполнить задачу быстрее за счёт большего количества участников. Много потоков здесь не нужно. Если доступно 4 ядра, то хватит 4 потоков.
					<br>Цель асинхронности - выполнить как можно больше задач в единицу времени.&nbsp;Для этого нужно много потоков и много одновременных задач. Тогда вероятность того, что процессорное время потратится на блокирующий вызов, гораздо ниже.
						<br>Поток - это всего лишь прокси к процессорным ресурсам и странно, что он используется по-разному. Виртуальные потоки решают эту проблему и сглаживают разницу между двумя подходами.
						</p>
						<hr class="" contenteditable="false">
							<a id="h-materialy-pro-korutiny-v-kotlin" class="heading-name ProseMirror-widget" contenteditable="false"></a>
							<h3>
								<span class="heading-content">
									<strong>Материалы про корутины в Котлин </strong>
								</span>
							</h3>
							<p>(уже реализованные виртуальные потоки на уровне байт-кода):</p>
							<p>1.&nbsp;
								<a href="https://www.youtube.com/watch?v=ffIVVWHpups" hreftransformed="" rel="noopener noreferrer nofollow">Андрей Бреслав — Асинхронно, но понятно. Сопрограммы в Kotlin</a>&nbsp;(56:51)
								<br>Отличное введение в тему
								</p>
								<p>2.&nbsp;
									<a href="https://kt.academy/article/cc-why" hreftransformed="" rel="noopener noreferrer nofollow">Why using Kotlin Coroutines?</a>&nbsp;(англ)
								</p>
								<p>Где и как использовать корутины в Андроид,&nbsp;на фронтенде и бэкенде,&nbsp;разница с реактивным подходом</p>
								<p>3. Подробная серия статей&nbsp;(англ)</p>
								<p>
									<a href="https://victorbrandalise.com/coroutines-part-i-grasping-the-fundamentals/" hreftransformed="" rel="noopener noreferrer nofollow">Часть 1: Grasping the Fundamentals</a>
									<br>
										<a href="https://victorbrandalise.com/coroutines-part-ii-job-supervisorjob-launch-and-async/" hreftransformed="" rel="noopener noreferrer nofollow">Часть 2: Job, SupervisorJob, Launch and Async</a>
                                    <br>
                                        <a href="https://victorbrandalise.com/coroutines-part-iii-structured-concurrency-and-cancellation/" hreftransformed="" rel="noopener noreferrer nofollow">Часть 3: Structured Concurrency and Cancellation</a>
                                </p>
                            </div>
                        </div>
