[[Содержание]](../../index.md)

# Вводная лекция

[[Назад]](../../foundation/code_strategies/code_strategies.md)  [[Вперед]](../executors/executors.md)

![async_tasks_review](async_tasks_review.mp4)
<div>
	<div>
		<a id="h-dopolnitelьnye-materialy" class="heading-name ProseMirror-widget" contenteditable="false"></a>
		<h3>
			<span class="heading-content">Дополнительные материалы</span>
		</h3>
		<p>1. Паттерн async/await - как асинхронность выглядит в других языках:
			<br>
				<a href="https://medium.com/front-stories/%D0%BA%D0%B0%D0%BA-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D0%B5%D1%82-async-await-%D0%B2-javascript-fe8f04cca415" hreftransformed="" rel="noopener noreferrer nofollow">Как работает Async/Await в JavaScript</a>
				<br>
					<a href="https://metanit.com/sharp/tutorial/13.3.php" hreftransformed="" rel="noopener noreferrer nofollow">Асинхронные методы, async и await в С#</a>
					<br>2.&nbsp;
						<a href="https://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/" hreftransformed="" rel="noopener noreferrer nofollow">What Color is Your Function?</a>&nbsp;(англ)
						<br>Известная статья про недостатки подхода async-await.
							<br>Суть: когда многопоточность реализована на уровне клиентского кода,&nbsp;приходится смешивать последовательные методы и асинхронные. Это неудобно и можно легко ошибиться. Поэтому очень круто иметь многопоточность на уровне платформы, а код писать в едином стиле, не разделяя его на синхронный и асинхронный.
							</p>
						</div>
					</div>

<div>
	<div>
		<div>
			<div>
				<h3>
					<span class="heading-content">Кодовая база для заданий 2 модуля:</span>
				</h3>
			</div>
		</div>
	</div>
</div>

[2nd_module.patch (22.3 kB)](../../patches/2nd_module.patch)

<div>
	<div>
		<p>Как применить изменения: </p>
		<ul>
			<li class="counter-1">
				<p>Сохранить файлик</p>
				<p>Если файл открылся в отдельной вкладке, нажмите правой кнопкой по тексту и выберите 
					<code spellcheck="false">Save as</code>
				</p>
				<p>Убедитесь, что сохранённый файл имеет расширение 
					<code spellcheck="false">.patch</code>
				</p>
			</li>
			<li class="counter-1">
				<p>В Intellij IDEA: Git (в самом верху) → Patch → Apply patch …</p>
				<p>Если в проекте нет git: VCS → Apply Patch…</p>
			</li>
			<li class="counter-1">
				<p>Далее выбрать файл с патчем</p>
			</li>
		</ul>
		<p>
			<br>
			</p>
			<p>Если проект не собирается, можно добавить тестам @Disabled, перезапустить IDEA и попробовать ещё раз</p>
			<p>
				<br>
				</p>
			</div>
		</div>

<div>
	<div>
		<div>
			<span>Задание 1. Практика: проверяем конфигурацию сервера</span>
		</div>
	</div>
	<div>
		<div>
			<div>
				<div>
					<div>
						<p>В этом задании вам нужно посмотреть реальную конфигурацию сервера. Выберите вариацию, исходя из ваших возможностей:</p>
						<ul>
							<li>
								<p>Вариант 1: у вас есть доступ к продакшену</p>
							</li>
						</ul>
						<p>Напишите тип сервера (Tomcat/Netty/Jetty/WebLogic/…), размер пула потоков (верхний лимит) и количество оперативной памяти на сервере в основной конфигурации</p>
						<ul>
							<li>
								<p>Вариант 2: у вас есть рабочий проект</p>
							</li>
						</ul>
						<p>Напишите  тип сервера (Tomcat/Netty/Jetty/WebLogic/…), на котором вы локально запускаетесь, размер пула потоков (верхний лимит) и количество оперативной памяти вашего компьютера</p>
						<ul>
							<li>
								<p>Вариант 3: если рабочего проекта нет</p>
							</li>
						</ul>
						<p>Напишите максимальный размер пула потоков по умолчанию в Spring Boot приложении</p>
						<p>
							<br>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>