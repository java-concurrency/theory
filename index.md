# Многопоточное программирование
[[Вперед]](about/about.md)
## Содержание
### Введение 
* [О курсе](about/about.md)
### Фундамент
* [Поток](foundation/thread/thread.md)
* [Модель памяти](foundation/java_memory_model/java_memory_model.md)
* [Подходы к написанию кода](foundation/code_strategies/code_strategies.md)
### Асинхронные задачи
* [Вводная лекция](async_tasks/async_tasks_review/async_tasks_review.md)
* [Executors](async_tasks/executors/executors.md)
* [CompletableFuture](async_tasks/completable_future/completable_future.md)
* [Виртуальные потоки](async_tasks/virtual_threads/virtual_threads.md)
### Общие переменные
* [Вводная лекция](common_variables/common_variables_review/common_variables_review.md)
* [Ключевое слово synchronized](common_variables/synchronized/synchronized.md)
* [Lock и его реализации](common_variables/locks/locks.md)
* [Deadlock, livelock, starvation](common_variables/deadlock_livelock_starvation/deadlock_livelock_starvation.md)
* [Модификатор volatile](common_variables/volatile/volatile.md)
* [Классы Atomic](common_variables/atomic_classes/atomic_classes.md)
* [Проверочная работа №1](common_variables/home_work_1/home_work_1.md)
* [Класс ThreadLocal](common_variables/thread_local/thread_local.md)
* [Semaphore, Exchanger](common_variables/semaphore_exchanger/semaphore_exchanger.md)
* [CountDownLatch, CyclicBarrier, Phaser](common_variables/barrier/barrier.md)
* [Многопоточные коллекции](common_variables/concurrent_collections/concurrent_collections.md)
* [Лабораторная работа: средства синхронизации](common_variables/lab_work/lab_work.md)
* [Неизменяемые переменные](common_variables/final_variables/final_variables.md)
### Обработка данных
* [ForkJoinPool для начинающих](data_processing/fork_join_poll_review/fork_join_poll_review.md)
* [ForkJoinPool для продолжающих](data_processing/fork_join_poll/fork_join_poll.md)
* [ForkJoinPool на практике](data_processing/in_practice/in_practice.md)
### Потоки данных
* [Вводная лекция](threads/threads_review/threads_review.md)
* [Очереди](threads/queue/queue.md)
* [Реактивное программирование](threads/reactive_programming/reactive_programming.md)
* [Проверочная работа №2](threads/home_work_2/home_work_2.md)
### Тестирование и мониторинг
* [Тестирование многопоточных приложений](testing/testing.md)
* [Метрики и мониторинг](monitoring/monitoring.md)
### Заключение
* [Что дальше?](conclusion/сonclusion.md)
* [Немного практики](practice/practice.md)
  
 
<br/>
  <p>
  <mark>* в Chrome наблюдаются ошибки воспроизведения видео, предпочтительнее использовать Firefox, Opera, Яндекс.Браузер.</mark>
  </p>