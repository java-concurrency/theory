[[Содержание]](../../index.md)

# Подходы к написанию кода

[[Назад]](../java_memory_model/java_memory_model.md)  [[Вперед]](../../async_tasks/async_tasks_review/async_tasks_review.md)

![code_strategies](code_strategies.mp4)

<div contenteditable="false">
	<h2>
		<span>Дополнительные материалы</span>
	</h2>
	<h3>
		<span>Для начинающих</span>
	</h3>
	<ul>
		<li>
			<p>Книга&nbsp;M.Kleppmann «Designing Data Intensive Applications»</p>
		</li>
		<li>
			<p>Большой сайт с&nbsp;паттернами и&nbsp;статьями про микросервисы:&nbsp;
				<a href="https://microservices.io/index.html" hreftransformed="" rel="noopener noreferrer nofollow">microservices.io</a>
			</p>
		</li>
	</ul>
	<hr />
    <h3>
		<span>Для продолжающих</span>
	</h3>
		<p>Многие компании подробно пишут про свои кейсы и&nbsp;инженерные решения. Такие блоги легко найти по&nbsp;запросу
			<br>
				<code spellcheck="false">XXX engineering blog</code>
			</p>
			<p>Английские:&nbsp;
				<a href="https://netflixtechblog.medium.com/" hreftransformed="" rel="noopener noreferrer nofollow">Netflix</a>,&nbsp;
				<a href="https://engineering.linkedin.com/blog" hreftransformed="" rel="noopener noreferrer nofollow">LinkedIn</a>,&nbsp;
				<a href="https://engineering.fb.com/" hreftransformed="" rel="noopener noreferrer nofollow">Facebook</a>
				<br>Русские:&nbsp;
					<a href="https://habr.com/ru/company/odnoklassniki/blog/" hreftransformed="" rel="noopener noreferrer nofollow">Одноклассники</a>,&nbsp;
					<a href="https://habr.com/ru/company/vk/blog/" hreftransformed="" rel="noopener noreferrer nofollow">VK</a>, хаб&nbsp;
					<a href="https://habr.com/ru/hub/hi/" hreftransformed="" rel="noopener noreferrer nofollow">Высокая производительность</a>&nbsp;на&nbsp;Хабре
				</p>
				<p>
					<br>
					</p>
					<p>Большинство таких статей очень специфические и&nbsp;подробные, но&nbsp;можно работать с&nbsp;ними по&nbsp;следующей схеме:
						<br>1. Выбираете статью с&nbsp;понятным заголовком и&nbsp;актуальной вам темой
							<br>Пример из&nbsp;блога FB: «FOQS: Making a&nbsp;distributed priority queue disaster-ready»
								<br>Ок, что-то про распределённые очереди и&nbsp;отказоустойчивость
									<br>2. По&nbsp;диагонали пробегаетесь по&nbsp;тексту
										<br>3. Пытаетесь понять, в&nbsp;чём проблема, почему текущие решения не&nbsp;справились, и&nbsp;что в&nbsp;итоге было сделано
										</p>
										<p>Если область малознакомая, можно начать с&nbsp;более простых вопросов. Например:</p>
										<ul>
											<li>
												<p>Почему у&nbsp;фейсбука свой сервис, а&nbsp;не&nbsp;кластер Kafka?</p>
											</li>
											<li>
												<p>Как организована репликация в&nbsp;разных регионах?</p>
											</li>
											<li class="counter-1">
												<p>Как доставляются сообщения между разными регионами?</p>
											</li>
										</ul>
										<p>Ответы можно найти в&nbsp;ранних постах. Иногда достаточно прочитать введение, подписи под картинками и&nbsp;саммари</p>
										<hr class="" contenteditable="false">
											<p>AKKA</p>
											<ul>
												<li>
													<p>
														<a href="https://developer.lightbend.com/guides/akka-quickstart-java/index.html" hreftransformed="" rel="noopener noreferrer nofollow">Простой пример использования Akka для&nbsp;java</a>
													</p>
												</li>
												<li class="counter-1">
													<p>
														<a href="https://manuel.bernhardt.io/articles/" hreftransformed="" rel="noopener noreferrer nofollow">Блог Manuel Bernhardt</a>&nbsp;(англ)&nbsp;— много подробных статей с&nbsp;картинками и&nbsp;примерами кода
													</p>
												</li>
												<li class="counter-1">
													<p>
														<a href="https://www.youtube.com/watch?v=Cc2QtbjUX60" hreftransformed="" rel="noopener noreferrer nofollow">Вадим Цесько&nbsp;— Фреймворк Akka и&nbsp;его использование в&nbsp;Яндексе</a>&nbsp;(50:09).&nbsp;Сначала идут общие слова,&nbsp;с&nbsp;23:50 начинается непосредственно про Яндекс
													</p>
												</li>
												<li class="counter-1">
													<p>Christian Baxter «Mastering Akka» (англ)&nbsp;— самая цельная и&nbsp;полезная книга по&nbsp;Akka</p>
													<p>
														<br>
														</p>
													</li>
												</ul>
												<p>
													<br>
													</p>
												</div>