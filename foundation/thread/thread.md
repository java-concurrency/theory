[[Содержание]](../../index.md)

# Поток

[[Назад]](../../about/about.md)  [[Вперед]](../java_memory_model/java_memory_model.md)

![thread](thread.mp4)

<div>
	<div>
		<div>
			<div contenteditable="false">
				<a id="h-dopolnitelьnye-materialy-esli-hochetsya-uglubitьsya-v-temu" class="heading-name ProseMirror-widget" contenteditable="false"></a>
				<h3>
					<span class="heading-content">Дополнительные материалы (если хочется углубиться в тему)</span>
				</h3>
				<p>1.&nbsp;
					<a href="https://www.youtube.com/watch?v=kKigibHrV5I" hreftransformed="" rel="noopener noreferrer nofollow">Андрей Паньгин — Память Java процесса по полочкам</a>&nbsp;(59:19)
				</p>
				<p>Можно смело ставить на x2.&nbsp;Очень подробно про устройство памяти в JVM.</p>
				<p>2.&nbsp;
					<a href="https://www.youtube.com/watch?v=0pyZERLBZvQ" hreftransformed="" rel="noopener noreferrer nofollow">Андрей Паньгин — Всё, что вы хотели знать о стек-трейсах и хип-дампах</a>&nbsp;(1:53:00)
				</p>
				<p>Тоже комфортно слушать на&nbsp;x2. Много интересных, но не особо важных для практики тем:</p>
				<ul>
					<li>
						<p>Генерация и обработка исключений внутри JVM</p>
					</li>
					<li class="counter-1">
						<p>Два режима jstack и jmap</p>
					</li>
					<li>
						<p>Как jstack взаимодействует с JVM</p>
					</li>
					<li>
						<p>API для извлечения стек-трейса в java 9</p>
					</li>
				</ul>
				<p>3.&nbsp;
					<a href="https://alidg.me/blog/2019/6/21/tlab-jvm" hreftransformed="" rel="noopener noreferrer nofollow">Thread-Local Allocation Buffers in JVM</a>&nbsp;(англ)
				</p>
				<p>Подробная статья про работу TLAB</p>
			</div>
		</div>
	</div>
</div>