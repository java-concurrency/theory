[[Содержание]](../../index.md)

# Модель памяти

[[Назад]](../thread/thread.md)  [[Вперед]](../code_strategies/code_strategies.md)

<div contenteditable="false">
	<h3>
		<span>Про эту лекцию и другие лекции о модели памяти</span>
	</h3>
	<p>Модель памяти&nbsp;существует, чтобы облегчить жизнь разработчику.&nbsp;Она задаёт&nbsp;правила работы с переменными в многопоточной среде: где можно положиться на JVM,&nbsp;а где предпринять дополнительные меры.&nbsp;Чтобы писать корректный код, важно знать эти правила и гарантии. Этим мы&nbsp;будем заниматься на протяжении всего курса.</p>
	<p>
		<br>
		</p>
		<p>Многие&nbsp;видели лекции Алексея Шипилёва про модель памяти. В них он рассказывает про внутренние детали, как это реализовано внутри JVM. Это многих отпугивает от многопоточки,&nbsp;потому что это сложно.
			<br>
				<br>
				</p>
				<p>Есть инструменты многопоточки, которыми пользуются разработчики - 
					<code spellcheck="false">synchronized</code>, 
					<code spellcheck="false">volatile</code>, классы&nbsp;
					<em>java.util.concurrent</em>. Они позволяют писать корректный код&nbsp;при правильном использовании.&nbsp;
				</p>
				<p>А есть реализация внутри JVM —&nbsp;про неё&nbsp;рассказывает Алексей.
					<br>
						<br>
						</p>
						<p>Пожалуйста, не путайте. Многопоточка — это не сложно. Java&nbsp;—&nbsp;дружелюбный для разработчиков язык.&nbsp;</p>
					</div>

![java_memory_model](java_memory_model.mp4)

<div dir="auto" readonly="" class="sc-fvprJM iXCTBp">
	<div>
		<h3>
			<span>Дополнительные материалы (если хотите углубиться в тему ещё дальше)</span>
		</h3>
		<p>1.&nbsp;
			<a href="https://colin-scott.github.io/personal_website/research/interactive_latency.html" hreftransformed="" rel="noopener noreferrer nofollow">Сколько времени занимает доступ к памяти и передача по сети в разные года</a>
		</p>
		<p>Можно двигать ползунок и увидеть,&nbsp;как замедляется развитие аппаратных технологий&nbsp;😢</p>
		<p>2.&nbsp;
			<a href="https://www.youtube.com/watch?v=iB2N8aqwtxc" hreftransformed="" rel="noopener noreferrer nofollow">Алексей Шипилёв — Прагматика Java Memory Model</a>&nbsp;(1:55:21)
		</p>
		<p>
			<a href="https://www.youtube.com/watch?v=C6b_dFtujKo" hreftransformed="" rel="noopener noreferrer nofollow">Алексей Шипилёв — Близкие Контакты JMM-степени</a>&nbsp;(57:48)
		</p>
		<p>Эти доклады можно считать первоисточником остальных докладов про JMM на youtube.&nbsp;</p>
		<p>Если вы посмотрели эти видео и приуныли, что всё сложно и непонятно, пересмотрите моё видео и текст перед ним</p>
		<p>3.&nbsp;
			<a href="https://shipilev.net/blog/2014/safe-public-construction/" hreftransformed="" rel="noopener noreferrer nofollow">Safe Publication and Safe Initialization in Java</a>&nbsp;(англ)
		</p>
		<p>Подробный разбор всех реализаций Double-checked locking.</p>
		<p>4.&nbsp;
			<a href="http://www.cs.umd.edu/~pugh/java/memoryModel/jsr-133-faq.html" hreftransformed="" rel="noopener noreferrer nofollow">JSR 133 (Java Memory Model) FAQ</a>&nbsp;(англ)
		</p>
		<p>Brian Goetz - Java Architect и&nbsp;автор книги Concurrency in Practice рассказывает простым языком,&nbsp;зачем нужна модель памяти и&nbsp;reordering,&nbsp;что происходит при синхронизации&nbsp;и т.д. Текст 2004 года,&nbsp;но основные идеи всё ещё актуальны.</p>
	</div>
</div>