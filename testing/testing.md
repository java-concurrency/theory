[[Содержание]](../index.md)

# Тестирование многопоточных приложений

[[Назад]](../threads/home_work_2/home_work_2.md)  [[Вперед]](../monitoring/monitoring.md)

![testing](testing.mp4)

<div dir="auto">
	<div contenteditable="false">
		<a contenteditable="false"></a>
		<h2>Дополнительные материалы</h2>
		<p>1.&nbsp;
			<a href="https://github.com/openjdk/jdk/tree/739769c8fc4b496f08a92225a12d07414537b6c0/test/jdk/java/util/concurrent/BlockingQueue" rel="noopener noreferrer nofollow">"Многопоточные" тесты очередей в JDK</a>
		</p>
		<p>2.&nbsp;
			<a href="https://mapdb.org/blog/thread_weaver/" rel="noopener noreferrer nofollow">Опыт использования библиотеки Thread Weaver</a>
		</p>
		<p>3.&nbsp;
			<a href="https://www.testcontainers.org/" rel="noopener noreferrer nofollow">Библиотека Testcontainers</a>
		</p>
		<p>Запускает сервисы в докер образах для интеграционных тестов</p>
		<p>4.&nbsp;
			<a href="https://ducktape-docs.readthedocs.io/en/latest/index.html" rel="noopener noreferrer nofollow">Библиотека Ducktape</a>
		</p>
		<p>Более масштабная библиотека, организует настройку окружения и выполнение интеграционных тестов</p>
		<hr contenteditable="false" />
		<a contenteditable="false"></a>
		<h3>Тестирование в Apache Kafka</h3>
		<p>В проекте очень прозрачные процессы, поэтому я&nbsp;так выделяю этот проект и рекомендую изучить их опыт:)</p>
		<p>1.&nbsp;
			<a href="https://cwiki.apache.org/confluence/display/KAFKA/Performance+testing" rel="noopener noreferrer nofollow">Метрики и инструменты для нагрузочного тестирования
				<br />
			</a>2.&nbsp;
			<a href="https://github.com/apache/kafka/tree/trunk/tests" rel="noopener noreferrer nofollow">Папка с тестами в проекте</a>
		</p>
	</div>
</div>

<div dir="auto">
	<div contenteditable="false">
		<p>Очень популярный вопрос на собеседованиях с упором в многопоточку: написать реализацию блокирующей многопоточной очереди. 
			<br />Классический вариант - использовать synchronized + wait/notify. Но можно продемонстрировать прогрессивные взгляды и использовать что-то другое.
			<br />
			<br />
			<mark>Задание:</mark>
			<br />1. Реализовать блокирующую многопоточную очередь с ограниченным размером. Основные методы:
		</p>
		<pre>
			<code spellcheck="false">
public void enqueue(T value)
public T dequeue()</code>
		</pre>
	</div>
	<p>(постарайтесь написать код, не смотря в исходники или готовые решения, как будто это лайвкодинг)</p>
	<p>2. Написать тесты для своего решения</p>
	<p>(на собесах это не требуется, но для закрепления навыков многопоточки - самое то)</p>
</div>undefined</div>

[Blocking_queue_implementation.patch (7.5 kB)](../patches/Blocking_queue_implementation.patch)

